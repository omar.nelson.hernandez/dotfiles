#!/usr/bin/env bash

echo "Installing rofi conf"

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

mkdir -p ~/.config/rofi
ln -s ${SCRIPTPATH}/config.rasi ~/.config/rofi/config.rasi
ln -s ${SCRIPTPATH}/DarkBlue.rasi ~/.config/rofi/DarkBlue.rasi

echo "Installed rofi conf"
