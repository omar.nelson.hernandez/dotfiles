#!/usr/bin/env bash

L_UID=$(id -u)

echo "Installing rofi"

if [ ${L_UID} -ne 0 ]; then
  echo "This script needs root permissions"
  exit 0
fi

apt-get install -y rofi

echo "rofi installed"

exit 0
