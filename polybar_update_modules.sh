#!/usr/bin/env bash

echo "Installing polybar modules"

mkdir -p ~/bin 

curl -o ~/bin/polybar_ticker.sh https://raw.githubusercontent.com/pstadler/ticker.sh/master/ticker.sh
curl -o ~/bin/polybar_timer.sh https://raw.githubusercontent.com/jbirnick/polybar-timer/master/polybar-timer.sh
curl -o ~/bin/polybar_pulseaudio_control.sh https://raw.githubusercontent.com/marioortizmanero/polybar-pulseaudio-control/master/pulseaudio-control.bash

chmod u+x ~/bin/polybar_ticker.sh
chmod u+x ~/bin/polybar_timer.sh
chmod u+x ~/bin/polybar_pulseaudio_control.sh
